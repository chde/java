
class Tree {

	/**
	 * KAPSELUNG UND STRUKTURIERUNG VON KLASSEN
	 */

	  Object value;
	  Tree[] children;
	  public static void main(String[] argv) {
	    Tree tree1 = new Tree();
	    Tree tree2 = new Tree();
	    Tree tree3 = new Tree();
	    Tree tree4 = new Tree();
	    tree1.children = new Tree[2];
	    tree2.children = new Tree[2];
	    tree3.children = new Tree[2];
	    tree4.children = new Tree[1];
	    Object o = new Object();
	    tree1.value = o;
	    tree2.value = o;
	    tree3.value = tree3.children;
	    tree4.value = tree4;
//	    tree1.children[0];
//	    tree1.children[1];
	    tree2.children[0] = tree3;
	    tree2.children[1] = tree4;
	    }
}
