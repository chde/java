Neues Projekt anlegen
=====================
Ins Projekt-Source-Verzeichnis wechseln

	cd /Users/chd/Applications/eclipse/workspace/

Initialisieren

	git init
	  Initialized empty Git repository in /Users/chd/Applications/eclipse/workspace/.git/

Projekt-Source-Verzeichnis mit Remote GIT verknuepfen

	git remote add origin ssh://git@bitbucket.org/chde/java.git

Datei erzeugen, bearbeiten und hinzufuegen

	echo "# This is my README" >> README.md
	git add README.md

Aenderungen comitten (ins lokale Repository)

	git commit -m "First commit. Adding a README."

Auf den Server hochspielen

	git push -u origin master

Bestehendes Projekt auschecken
==============================
Namen eintragen

	git config --global user.name "Christian Denz"
	git config --global user.email "chd@space.net"

Klonen

	git clone ssh://git@bitbucket.org/chde/java.git

Im Verzeichnis

	vi README.md

Comitten (ins lokale Repository)

	git commit -a -m "Kommentar"

Auf den Server hochspielen

	git push origin master

Fertig!
Vor der Bearbeitung die aktuellste Version holen:

	git pull origin master

